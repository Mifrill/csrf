<?php 
	session_start();

	$form_key = 'sendmessage_form';

	$csrf_token = '';

	if(version_compare(PHP_VERSION, '7.0.0', '>=')) {
		$csrf_token = bin2hex(random_bytes(32));
	}else {
		if ( function_exists('mcrypt_create_iv')) {
			$csrf_token = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
		}else {
			$csrf_token = bin2hex(openssl_random_pseudo_bytes(32));
		}
	}

	$_SESSION['csrf_' . $form_key] = $csrf_token;
 ?>
<form action="action.php" method="POST">
	<input type="text" name="message" placeholder="Message text">
<hr>
<input type="hidden" name="csrf" value="<?php echo $csrf_token; ?>">
<input type="submit" name="do_submit" value="Send a message">
</form>
